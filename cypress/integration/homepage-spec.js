describe('the homepage', () => {
  beforeEach(() => {
    cy.exec('npm run test:db:seed')
  })

  it('renders the agent status data', function () {
    cy.visit('/')

    cy.contains('Agent Tracker')
    cy.contains('418a0319-842f-4244-a460-25e0beb00f65')
    cy.contains('55527040-c934-4d6c-b3da-9bf69b0792cb')
  })

  it('renders new agent status data', function () {
    cy.visit('/')

    cy.exec('npm run test:ws:agent-update')
    cy.contains('8c2ba66e-a378-43ad-a72a-09f9b6bef107')
  })

  it('renders updates of agent statuses', function () {
    cy.visit('/')
    cy.contains('0 km/hr')

    cy.exec('npm run test:ws:agent-updates')
    cy.contains('418a0319-842f-4244-a460-25e0beb00f65')
    cy.contains('55527040-c934-4d6c-b3da-9bf69b0792cb')
    cy.contains('8c2ba66e-a378-43ad-a72a-09f9b6bef107')
    cy.contains('0.2627188366399375 km/hr')
    cy.contains('2.7139100740303626 km/hr')
    cy.contains('31.39279613238567 km/hr')
  })
})
