const onError = (error, bound) => {
  if (error.syscall !== 'listen') {
    throw error
  }

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bound + ' requires elevated privileges')
      process.exit(1)
      break
    case 'EADDRINUSE':
      console.error(bound + ' is already in use')
      process.exit(1)
      break
    default:
      throw error
  }
}

module.exports = onError
