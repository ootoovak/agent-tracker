const debug = require('debug')('agent-tracker:server')
const udp = require('dgram')
const msgpack = require('msgpack-lite')
const WebSocket = require('ws')
const agentDb = require('../../app/agent-db')
const errorReporter = require('./error-reporter')

const startServer = (port, wss) => {
  const server = udp.createSocket('udp4')
  server.bind(port)

  const broadcastUpdateToAllWebSocketClients = () => {
    wss.clients.forEach(function each (client) {
      if (client.readyState === WebSocket.OPEN) {
        let agentData = agentDb.all()
        client.send(JSON.stringify(agentData))
      }
    })
  }

  const onMessage = (message, info) => {
    let agentUpdate = msgpack.decode(message)
    agentDb.upsert(agentUpdate)
    broadcastUpdateToAllWebSocketClients()
  }

  const onListening = () => {
    debug('UDP server listening on ' + port)
  }

  const onError = error => {
    errorReporter(error, port)
  }

  server.on('message', onMessage)
  server.on('listening', onListening)
  server.on('error', onError)

  return server
}

module.exports = { startServer }
