const debug = require('debug')('agent-tracker:server')
const http = require('http')
const app = require('../../app')
const errorReporter = require('./error-reporter')

const startServer = port => {
  app.set('port', port)

  let server = http.createServer(app)
  server.listen(port)

  const onListening = () => {
    debug('HTTP server listening on ' + port)
  }

  const onError = error => {
    errorReporter(error, port)
  }

  server.on('error', onError)
  server.on('listening', onListening)

  return server
}

module.exports = { startServer }
