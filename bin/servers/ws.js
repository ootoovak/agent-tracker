const WebSocket = require('ws')

const startServer = server => {
  let wss = new WebSocket.Server({ server })
  return wss
}

module.exports = { startServer }
