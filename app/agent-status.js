const haversine = require('haversine')
const luxon = require('luxon')

/**
 * Agent is considered not moving if it drops below this speed at any time.
 */
const MINIMUM_SPEED = 0.36

const build = (agentUpdate, speed = 0.0, moving = false) => {
  let agentStatus = Object.assign({}, agentUpdate)
  agentStatus.speed = speed
  agentStatus.moving = moving
  return agentStatus
}

const calculateFromPrevious = (lastAgentStatus, agentUpdate) => {
  let lastTimestamp = luxon.DateTime.fromISO(lastAgentStatus.timestamp)
  let newTimestamp = luxon.DateTime.fromISO(agentUpdate.timestamp)
  let hoursPassed = newTimestamp.diff(lastTimestamp).as('hours')
  let kmDistance = haversine(lastAgentStatus, agentUpdate)
  let speed = hoursPassed === 0.0 ? 0.0 : kmDistance / hoursPassed
  let moving = Math.abs(speed) > MINIMUM_SPEED
  return build(agentUpdate, speed, moving)
}

const update = (lastAgentStatus, agentUpdate) => {
  let agentStatus
  if (lastAgentStatus === undefined) {
    agentStatus = build(agentUpdate)
  } else {
    agentStatus = calculateFromPrevious(lastAgentStatus, agentUpdate)
  }
  return agentStatus
}

const sort = agentStatuses => {
  let data = Array.from(agentStatuses)

  return data.sort((a, b) => {
    let idA = a.id
    let idB = b.id

    if (idA < idB) {
      return -1
    }

    if (idA > idB) {
      return 1
    }

    return 0
  })
}

module.exports = { update, sort }
