const agentStatus = require('./agent-status')

let db = new Map()

const agentUpdate = {
  build: data => {
    return {
      id: data[0],
      latitude: data[1],
      longitude: data[2],
      timestamp: data[3]
    }
  }
}

const upsert = data => {
  let update = agentUpdate.build(data)
  let lastStatus = db.get(update.id)
  let status = agentStatus.update(lastStatus, update)
  db.set(status.id, status)
}

const all = () => {
  let agents = agentStatus.sort(db.values())
  return agents
}

const clear = () => {
  db.clear()
}

module.exports = { upsert, all, clear }
