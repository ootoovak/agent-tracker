FROM node:12

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm ci --only=production

COPY . .

EXPOSE 8088/tcp
EXPOSE 9099/udp

CMD ["node", "bin/agent-tracker"]
