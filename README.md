# Agent Tracker

A web application to monitor Agent activity. It receives agent updates via UDP encoded with [MessagePack](https://msgpack.org/) and then updates them live to all clients using WebSockets.

I have left it up to the agents on how often they send updates as that might be specific to the agents use case but I chose UDP for because it is lighter weight than TCP and given there will be constant updates it doesn't matter as much if one message happens to be dropped as it will be replaced by the newer one in the next update anyhow. I also chose to encode with MessagePack instead of JSON for reasons of it being lighter weight as well as having implementations in many different languages so it shouldn't be an issue what language the agents software is written in.

The agents are expected to send their information in the following format:

```json
[
  "8c2ba66e-a378-43ad-a72a-09f9b6bef107",
  41.985556,
  -78.037852,
  "2019-10-10T09:12:11.457599Z"
]
```

Examples of this can be found in the `scripts/` folder specifically in the files:

- `scripts/db-seed.js`
- `scripts/ws-agent-update.js`
- `scripts/ws-agent-updates.js`

And once you have the server running those scripts can be run as follows:

- `npm run test:db:seed`
- `npm run test:ws:agent-update`
- `npm run test:ws:agent-updates`

## Assumptions

For the requirement:
_You should visually highlight the agents that have not been moving for more than 10 seconds (the agent sent updates, but didn't move more that 1 meter)._

I've interpreted this to mean that the done has dropped below a minimum speed of moving less than 1 meter in 10 seconds or more, which is the equivalent of a minimum speed of 0.36 km/hr.

## Running the application

### Build

This builds a application [Docker](https://www.docker.com/) image:

```bash
docker build -t ootoovak/agent-tracker .
```

### Run

This runs the Docker container exposing both the TCP and UDP ports to `8088` and `9099` respectively:

```bash
docker run -p 8088:8088 -p 9099:9099/udp -d ootoovak/agent-tracker
```

### Stopping

Find the Container ID with `docker ps` then stop the container with the following command:

```bash
docker stop <container_id>
```

## Testing the application

### Run all tests

_Note: Make sure you are not running the development or production servers locally before running the tests._

This will run all tests. The web backend tests that use [Jest](https://jestjs.io/) and the end-to-end tests that use [Cypress](https://www.cypress.io/).

```bash
npm test
```

### Web server tests only

Running only the Jest tests.

```bash
npm run test:backend
```

### End-to-end tests

_Note: Make sure you are not running the development or production servers locally before running the tests._

Running only the Cypress tests.

```bash
npm run test:feature`
```

## Development

This project is linted and formatted with [Prettier-Standard](https://github.com/sheerun/prettier-standard).

The dev server can be run with:

```bash
npm run dev:start
```
