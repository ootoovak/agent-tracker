const msgpack = require('msgpack-lite')
const luxon = require('luxon')
const haversine = require('haversine')

let db = new Map()

function decode (encodedData) {
  return msgpack.decode(encodedData)
}

function buildAgentUpdateFromData (data) {
  return {
    id: data[0],
    latitude: data[1],
    longitude: data[2],
    timestamp: data[3]
  }
}

function buildAgentStatus (agentUpdate, speed = 0.0, moving = false) {
  let agentStatus = Object.assign({}, agentUpdate)
  agentStatus.speed = speed
  agentStatus.moving = moving
  return agentStatus
}

function calculateAgentStatus (lastAgentStatus, agentUpdate) {
  let lastTimestamp = luxon.DateTime.fromISO(lastAgentStatus.timestamp)
  let newTimestamp = luxon.DateTime.fromISO(agentUpdate.timestamp)
  let hoursPassed = newTimestamp.diff(lastTimestamp).as('hours')
  let kmDistance = haversine(lastAgentStatus, agentUpdate)
  let speed = kmDistance / hoursPassed
  let moving = speed !== 0.0
  return buildAgentStatus(agentUpdate, speed, moving)
}

function updateAgentStatus (agentUpdate) {
  let agentStatus
  if (db.has(agentUpdate.id)) {
    let lastAgentStatus = db.get(agentUpdate.id)
    agentStatus = calculateAgentStatus(lastAgentStatus, agentUpdate)
  } else {
    agentStatus = buildAgentStatus(agentUpdate)
  }
  db.set(agentStatus.id, agentStatus)
}

function sortAgentStatuses (agentStatuses) {
  let data = Array.from(agentStatuses)

  return data.sort((a, b) => {
    let idA = a.id
    let idB = b.id

    if (idA < idB) {
      return -1
    }

    if (idA > idB) {
      return 1
    }

    return 0
  })
}

module.exports = {
  upsert: encodedData => {
    let data = decode(encodedData)
    let agentUpdate = buildAgentUpdateFromData(data)
    updateAgentStatus(agentUpdate)
  },
  all: () => {
    return sortAgentStatuses(db.values())
  }
}
