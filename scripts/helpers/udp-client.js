const udp = require('dgram')
const msgpack = require('msgpack-lite')

const udpHost = process.env.UDP_HOST || 'localhost'
const udpPort = process.env.UDP_PORT || '9099'

const sendMessages = messages => {
  for (let message of messages) {
    sendMessage(message)
  }
}

const sendMessage = message => {
  let udpClient = udp.createSocket('udp4')

  let data = msgpack.encode(message)

  udpClient.send(data, 0, data.length, udpPort, udpHost, (error, bytes) => {
    if (error) throw err
    udpClient.close()
  })
}

module.exports = { sendMessage, sendMessages }
