const udpClient = require('./helpers/udp-client')

let messages = [
  [
    '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
    41.985656,
    -78.037452,
    '2019-10-10T09:12:15.457599Z'
  ],
  [
    '418a0319-842f-4244-a460-25e0beb00f65',
    41.985552,
    -78.037859,
    '2019-10-10T09:12:21.457599Z'
  ],
  [
    '55527040-c934-4d6c-b3da-9bf69b0792cb',
    41.985156,
    -78.037852,
    '2019-10-10T09:13:10.457599Z'
  ]
]

udpClient.sendMessages(messages)
