const udpClient = require('./helpers/udp-client')

let messages = [
  [
    '55527040-c934-4d6c-b3da-9bf69b0792cb',
    41.985556,
    -78.037852,
    '2019-10-10T09:12:11.457599Z'
  ],
  [
    '418a0319-842f-4244-a460-25e0beb00f65',
    41.985556,
    -78.037852,
    '2019-10-10T09:12:11.457599Z'
  ]
]

udpClient.sendMessages(messages)
