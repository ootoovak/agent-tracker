function updateAgentStatuses (data) {
  let tableBody = document.querySelector('#js-agent-statuses tbody')
  let newRows = ''
  for (let row of data) {
    newRows += `<tr class='${row.moving ? '' : 'bg-warning'}'>`
    newRows += `<td>${row.id}</td>`
    newRows += `<td>${row.speed} km/hr</td>`
    newRows += `</tr>`
  }
  tableBody.innerHTML = newRows
}

function wsConnect () {
  let protocol = (window.location.protocol == 'https:' && 'wss://') || 'ws://'
  let wsUri = protocol + window.location.host + '/ws/'
  let ws = new WebSocket(wsUri)

  ws.onmessage = function (message) {
    let data = JSON.parse(message.data)
    updateAgentStatuses(data)
  }
}

document.addEventListener('DOMContentLoaded', wsConnect)
