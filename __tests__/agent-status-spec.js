const agentStatus = require('../app/agent-status')

describe('updating a agents status', () => {
  test('returns the first status if no previous ones are provided', () => {
    let update = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 39.985556,
      longitude: -77.037852,
      timestamp: '2019-10-10T09:12:09.457599Z'
    }
    let status = agentStatus.update(undefined, update)
    expect(status).toStrictEqual({
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 39.985556,
      longitude: -77.037852,
      timestamp: '2019-10-10T09:12:09.457599Z',
      speed: 0.0,
      moving: false
    })
  })

  test('returns a new agent status based off of the last one', () => {
    let lastStatus = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 39.985556,
      longitude: -77.037852,
      timestamp: '2019-10-10T09:12:09.457599Z',
      speed: 0.0,
      moving: false
    }
    let update = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 39.995566,
      longitude: -77.047862,
      timestamp: '2019-10-10T10:12:09.457599Z'
    }
    let status = agentStatus.update(lastStatus, update)
    expect(status).toStrictEqual({
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 39.995566,
      longitude: -77.047862,
      timestamp: '2019-10-10T10:12:09.457599Z',
      speed: 1.4021860476689092,
      moving: true
    })
  })
})

describe('flagging non-moving agents', () => {
  test('considered not moving if it has not moved more than 1 meter in over 10 seconds', () => {
    let originalStatus = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985556,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:00.457599Z',
      speed: 0.0,
      moving: false
    }
    let nextUpdate = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985564,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:11.457599Z'
    }
    let status = agentStatus.update(originalStatus, nextUpdate)
    expect(status).toStrictEqual({
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985564,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:11.457599Z',
      speed: 0.29112853499696395,
      moving: false
    })
  })

  test('considered moving if it has not moved more than 1 meter in over 10 seconds', () => {
    let originalStatus = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985556,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:00.457599Z',
      speed: 0.0,
      moving: false
    }
    let nextUpdate = {
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985566,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:11.457599Z'
    }
    let status = agentStatus.update(originalStatus, nextUpdate)
    expect(status).toStrictEqual({
      id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      latitude: 40.985566,
      longitude: -97.037852,
      timestamp: '2019-10-10T09:12:11.457599Z',
      speed: 0.36391066887549206,
      moving: true
    })
  })
})

describe('the sort order for agent updates', () => {
  test('sorts by the id', () => {
    let statuses = [
      { id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107' },
      { id: '418a0319-842f-4244-a460-25e0beb00f65' },
      { id: '55527040-c934-4d6c-b3da-9bf69b0792cb' }
    ]
    let result = agentStatus.sort(statuses)
    expect(result).toStrictEqual([
      { id: '418a0319-842f-4244-a460-25e0beb00f65' },
      { id: '55527040-c934-4d6c-b3da-9bf69b0792cb' },
      { id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107' }
    ])
  })
})
