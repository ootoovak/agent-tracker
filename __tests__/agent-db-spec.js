const agentDb = require('../app/agent-db')

describe('updating agent statues by adding agent updates', () => {
  beforeEach(() => {
    agentDb.clear()
  })

  test('adding a couple of new statuses', () => {
    let update1 = [
      '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      39.985556,
      -77.037852,
      '2019-10-10T09:12:09.457599Z'
    ]
    let update2 = [
      '55527040-c934-4d6c-b3da-9bf69b0792cb',
      41.985556,
      -78.037852,
      '2019-10-10T09:12:11.457599Z'
    ]
    agentDb.upsert(update1)
    agentDb.upsert(update2)
    let allStatuses = agentDb.all()
    expect(allStatuses).toStrictEqual([
      {
        id: '55527040-c934-4d6c-b3da-9bf69b0792cb',
        latitude: 41.985556,
        longitude: -78.037852,
        timestamp: '2019-10-10T09:12:11.457599Z',
        speed: 0.0,
        moving: false
      },
      {
        id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
        latitude: 39.985556,
        longitude: -77.037852,
        timestamp: '2019-10-10T09:12:09.457599Z',
        speed: 0.0,
        moving: false
      }
    ])
  })

  test('adding two updates for the same agent', () => {
    let update1 = [
      '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      39.985556,
      -77.037852,
      '2019-10-10T09:12:09.457599Z'
    ]
    let update2 = [
      '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
      39.985546,
      -77.037952,
      '2019-10-10T09:12:11.457599Z'
    ]
    agentDb.upsert(update1)
    agentDb.upsert(update2)
    let allStatuses = agentDb.all()
    expect(allStatuses).toStrictEqual([
      {
        id: '8c2ba66e-a378-43ad-a72a-09f9b6bef107',
        latitude: 39.985546,
        longitude: -77.037952,
        timestamp: '2019-10-10T09:12:11.457599Z',
        speed: 15.465750029971984,
        moving: true
      }
    ])
  })
})
