const express = require('express')
const router = express.Router()
let agentStatus = require('../app/agent-db')

router.get('/', function (req, res, next) {
  res.render('index', {
    title: 'Agent Tracker',
    agents: agentStatus.all()
  })
})

module.exports = router
